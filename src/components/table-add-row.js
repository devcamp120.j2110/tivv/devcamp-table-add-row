import { Grid, TableContainer, Table, TableHead, TableRow, TableCell, TableBody, Paper, Button, TextField } from "@mui/material";
import 'bootstrap/dist/css/bootstrap.min.css';
import { useState } from "react";
import { Col, Label, Row } from 'reactstrap'
function AddRow() {
    const [inputValue, setInputValue] = useState("");
    const [inputArray, setInputArry] = useState([]);
    const inputChange = (event) => {
        setInputValue(event.target.value);
    }
    const onBtnClick = () => {
        // console.log("Thêm dòng");
        // console.log(inputValue);
        setInputArry([...inputArray, inputValue]);
        console.log(inputArray);
    }

    return (
        <Grid container item sm={12} md={12} lg={12}>

            <Grid sx={{ width: 800, margin: 'auto', py: 3 }}>
                <Row className="bg-light">
                    <Col xs={3} className="p-3">
                        <Label>Nhập nội dung dòng:</Label>
                    </Col>
                    <Col xs={6} className="pt-1">
                        <TextField id="outlined-basic" label="Input Data" variant="outlined" fullWidth onChange={inputChange} />
                    </Col>
                    <Col xs={3} className="p-3">
                        <Button variant="contained" color="secondary" onClick={onBtnClick}>Thêm</Button>
                    </Col>
                </Row>
            </Grid>
            <TableContainer component={Paper}>
                <Table sx={{ minWidth: 650 }} aria-label="simple table">
                    <TableHead>
                        <TableRow>
                            <TableCell align="center" sx={{ fontWeight: 'bold' }}>STT</TableCell>
                            <TableCell align="center" sx={{ fontWeight: 'bold' }}>Nội dung</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {inputArray.map((row, index) => (
                        <TableRow
                            key={index}
                            sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                        >
                            <TableCell component="th" scope="row">
                                {index+1}
                            </TableCell>
                            <TableCell align="center">
                                {row}
                            </TableCell>

                        </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
        </Grid>
    )
}
export default AddRow;
import { Container } from "@mui/material";
import AddRow from "./components/table-add-row";



function App() {
  return (
    <Container>
      <AddRow/>
    </Container>
  );
}

export default App;
